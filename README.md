# rest-assured-api-module
A basic skeleton test framework using cucumber. Rest calls are handled by restAssured

# **Configuration required:**
- Java 8 , 1.8 jdk and jde is a must
- It will not work with any other Java
- You will need to have Maven installed
- You will need to have intelliJ add ons installed : “cucumber for java” and “gherkin”
- Ensure this is set correct:
    - Maven > Runner > ensure JRE in the drop down is set to project JDK 1.8
- This is only currently configured for API testing only
- Navigate to preferences > Build execution… > build tools > Maven > User settings file & set it to override to take in the settings.xml
of the project (or you can use your own local ones if you have it set in the .m2 folder)

- You need to ensure you have your .m2 folder & paths set

- You need to ensure you have a settings.xml set (either in the m2 folder or by setting the project to follow the specific ones)

- You need to ensure you have Maven downloaded and working

- You need to ensure you only have Java 8 invoked & your IntelliJ is pointing towards Java 1.8 everywhere

- You need to ensure you set JAVA_HOME, M2_HOME and the PATH, for example ensuring the below

       - export JAVA_HOME=/Library/Java/JavaVirtualMachines/jdk1.8.0_261.jdk/Contents/Home
       - export M2_HOME=/Users/<user>/Maven/apache-maven-3.6.3/
       - export PATH=$PATH:/Users/<user>/Maven/apache-maven-3.6.3/bin

**Install java 8 from this link** (you will have to register to download)
- https://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html


These commands help you move the JAVA 8 into the right place for the configuration:

- export JAVA_HOME=/Library/Java/Home
- echo $JAVA_HOME

Check what your MVN version is and it should be pointing to java 8:

- mvn -v

Point mvn to the correct Java version:

export JAVA_HOME=$(/usr/libexec/java_home) 

Run your locally created bash profile to auto config java with mvn:

source .bash_profile

**Creating the correct bash profile**

Go to terminal and run the following

    cd ~/ - This will get you into your home directory 
    
Then run the following command 

    touch .bash_profile - This will create your new bash profile
    
Then run the following command 

     open -e .bash_profile - This will open your bash profile and enable you to edit it  
     
Add the following into your bash profile 

     export JAVA_HOME=$(/usr/libexec/java_home -v 1.8)
     
      echo $JAVA_HOME
      
       mvn -v
       
       
This will create your bash script to export Java HOME and then check mvn is configured with it correctly 


Then you can run the bash profile whenever you need to configure

     source .bash_profile  - This will allow you to run your bash script when required

**to ensure git ignore file works correctly:**
- git rm -r --cached .

- git add .

- git commit -m "fixed untracked files"


# **Environments:**

**Adding more environments to the accepted list**
- src > main> java > utils > enums > environments

**Setting the environment**
- src > main> java > utils > setEnvironment


# Command to run tests:

**Individual tags example**:

` mvn clean verify -Papi_tests -Dcucumber.filter.tags="@test"`

 **Multiple tags example:**
 
 `mvn clean verify -Papi_tests -Dcucumber.filter.tags= "@test and @api"`

**POM profile to use:**
 
 `-Papi_tests (this runs multiple tests together in parallel, currently set to 5 tests)`

# **Locations & navigation help:**

 **Tags location:**
 - These are in the feature files and you can make them bespoke and run what you want

 **Feature files**:src > test > resources > features > api

**Java actions** (code for tests):
  src > test > java > api > actions

**Common methods in the utils section**:
  src > main > java > utils 

**gherkin step definitions** (code for tests):
  src > test > java > api > stepDefinition

**Json data payloads/ files**
  src > test > resources > jsonData

**properties for urls**
  src > test > resources > properties

**Location for where the main report is stored**
  target  > cucumber-report > cucumber-html-reports > features-overview.html - open this in the browser 
  
  **Constants file:**
  - src > main> java > utils > Constants
  
  **enums file -where environments and serenity session variables are held**
  - src > main> java > utils > enums
  
  **properties for getting the urls**
  - src > main> java > utils > PropertyUtils
  
  **Setting the environment**
  - src > main> java > utils > setEnvironment
  
  **Adding more environments to the accepted list**
  - src > main> java > utils > enums > environments
  
  **Common utils**
  - src > main> java > utils > commonUtils 
  
  **Using the session variables**
  - src > main> java > utils > SessionVariablesUtils 

# Checkstyle coding practice and style:
- checkstyle.xml - This is where we have inputted the coding practice we want to adhere to and this is run when you invoke the test to ensure it matches the standards and the build will fail if it doesn't

# POM:
- you want to make sure the glue points correctly to where the steps are otherwise they won't get picked up

# **Additional features:**
- You can integrate this with Jenkins and add a docker entry point to enable a CI pipeline
- There are configuration files attached to configure & download from scratch for somebody who has never had it before/ used automation.
      - FINAL steps to install & configure IntelliJ for macs.docx
      - FINAL steps to install & configure IntelliJ for windows.docx
- The tests which are built in this repository is from an open API thus is not maintained so can fail at any time
