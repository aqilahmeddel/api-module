@test
Feature: A an example feature file - sending calls to https://api.postcodes.io/postcodes

  Scenario: Get information on multiple postcodes
    Given I send a post request to postcode.io to search for the following postcodes
      | OX49 5NU |
      | M32 0JG  |
      | NE30 1DP |
    Then I check the response has the status code of 400

  Scenario Outline: Get information on individual postcodes
    Given I send a get request to postcode.io for the postcode "<postCode>"
    Then I check the response has the status code of 200
    And The response contains the field "result.country" with the value "England"
    And The response contains the field "result.region" with the value "<region>"

    Examples:
      | postCode | region          |
      | MK454AE  | East of England |
      | B147AN   | West Midlands   |
      | EX230LT  | South West      |

  Scenario: 1) Example scenario for sharing data between steps in different step def files
    Given I send a get request to postcode.io for the postcode "MK443NG"
    Then I check the response has the status code of 200

  Scenario: 2) Example scenario for sharing data between steps in different step def files
    Given I send a get request to postcode.io for the postcode "bob"
    Then I check the response has the status code of 404
