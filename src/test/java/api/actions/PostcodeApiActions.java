package api.actions;

import io.restassured.RestAssured;
import models.SessionData;
import org.json.JSONArray;
import org.json.JSONObject;
import utils.PropertyUtils;

import java.util.List;

import static utils.Constants.JSON_CONTENT_TYPE_HEADER;

public class PostcodeApiActions {

    private SessionData sessionData;

    public PostcodeApiActions(SessionData sessionData) {
        this.sessionData = sessionData;
    }

    // POST request
    public void sendAPostRequestToPostcodeIoForMultiplePostcodes(List<String> postcodes) {
        sessionData.setApiResponse(
                RestAssured
                        .given()
                    .header(JSON_CONTENT_TYPE_HEADER)
                    .body(createPostcodeRequestBody(postcodes).toString())
                    .when()
                    .post(PropertyUtils.getPostCodeIoUrl()));
    }


    // GET request
    public void sendGetRequestToPostCodeIoForPostcodeOf(String postcode) {
        sessionData.setApiResponse(RestAssured.given()
                .when()
                .get(PropertyUtils.getPostCodeIoUrl() + postcode));
    }

    private JSONObject createPostcodeRequestBody(List<String> postcodes) {
        return new JSONObject().put("postcodes", new JSONArray(postcodes));
    }

}
