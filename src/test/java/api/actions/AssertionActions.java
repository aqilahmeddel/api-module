package api.actions;

import models.SessionData;
import org.hamcrest.CoreMatchers;
import org.junit.Assert;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.assertThat;

public class AssertionActions {

    private SessionData sessionData;

    Logger logger = LoggerFactory.getLogger(AssertionActions.class.getName());

    public AssertionActions(SessionData sessionData) {
        this.sessionData = sessionData;
    }

    // Checks the response code is of value "code"
    public void validateResponseCode(int code) {
        Assert.assertEquals(sessionData.getApiResponse().then().extract().statusCode(), code);
    }

    //Allows you to assert an object with an actual in any method
    public static void assertObjects(Object expected, Object actual) {
        Assert.assertEquals(expected, actual);
    }

    //Assert response has the correct value in the correct field
    public void theResponseHasTheCorrectStringValueInTheCorrectField(String field, String value) {

        logger.info("field, {}", field);
        logger.info("response body value at field, {}", sessionData.getApiResponse().then().extract().path(field).toString());
        logger.info("value asserting, {}", value.toString());
        assertObjects(value, sessionData.getApiResponse().then().extract().path(field));
    }

    //Assert response has the correct value in the correct field
    public void theResponseHasTheCorrectIntValueInTheCorrectField(String field, int value) {

        logger.info("field, {}", field);
        logger.info("response body value at field, {}", sessionData.getApiResponse().then().extract().path(field).toString());
        logger.info("value asserting, {}", value);
        assertObjects(value, sessionData.getApiResponse().then().extract().path(field));
    }

    //This is for checking the response body returned contains a string
    public void responseBodyContainsString(String stg) {
        sessionData.getApiResponse().then().assertThat().body(CoreMatchers.containsString(stg));
    }

    //This is for checking the response body returned does not contain a string
    public void responseBodyDoesNotContainString(String stg) {
        sessionData.getApiResponse().then().assertThat().body(CoreMatchers.not(containsString(stg)));
    }

    //This is for checking the response body has a particular path populated (not null)
    public void responseContainsNotNullKeyValue(String field) {
        logger.info("field, {}", field);
        logger.info("response body value at field, {}", sessionData.getApiResponse().then().extract().path(field).toString());
        assertThat("key has a not null value", sessionData.getApiResponse().then().extract().path(field), notNullValue());
    }

    //This is for checking the response body has a particular path as null
    public void responseContainsNullKeyValue(String field) {
        logger.info("field, {}", field);
        logger.info("response body value at field, {}", sessionData.getApiResponse().then().extract().path(field).toString());
        assertThat("key has a null value", sessionData.getApiResponse().then().extract().path(field), nullValue());
    }


}



