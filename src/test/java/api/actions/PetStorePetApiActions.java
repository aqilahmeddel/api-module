package api.actions;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import models.SessionData;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import utils.PropertyUtils;
import utils.GetSessionVariablesUtils;
import utils.sessionVariables.SessionVariable;

import static utils.CommonUtils.generateRandomNumeric;
import static utils.Constants.JSON_CONTENT_TYPE_HEADER;
import static utils.Constants.PET_ENDPOINT;
import static utils.enums.SessionVariablesKey.Id;

public class PetStorePetApiActions {

    private Response response;
    Logger logger = LoggerFactory.getLogger(PetStorePetApiActions.class.getName());
    private SessionData sessionData;

    public PetStorePetApiActions(SessionData sessionData) {
        this.sessionData = sessionData;
    }

    public JSONObject petPayload(String name, String status) {

        JSONObject pet = new JSONObject();

        String randomId = generateRandomNumeric(6);
        SessionVariable.setSessionVariable(Id).to(randomId);

        pet.put("id", randomId);
        pet.put("name", name);
        pet.put("status", status);

        logger.info("pet payload created, {}", pet.toString());
        return pet;
    }

    //create a new pet
    public void createAPet(JSONObject payload) {
        setResponse(
                RestAssured
                        .given()
                        .when()
                        .body(payload.toString())
                        .header(JSON_CONTENT_TYPE_HEADER)
                        .post(PropertyUtils.getPetstoreUrl() + PET_ENDPOINT));

        sessionData.setApiResponse(getResponse());

        logger.info("url used, {}", (PropertyUtils.getPetstoreUrl() + PET_ENDPOINT).toString());
    }

    //this is for retrieving the newly created pet
    public void retrieveThePetCreated() {

        setResponse(
                RestAssured
                        .given()
                        .when()
                        .get(PropertyUtils.getPetstoreUrl() + PET_ENDPOINT + GetSessionVariablesUtils.getPetId()));


        sessionData.setApiResponse(getResponse());
    }

    private Response getResponse() {
        return response;
    }

    private void setResponse(Response response) {
        this.response = response;
    }
}
