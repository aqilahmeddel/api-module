package api.steps;

import api.actions.AssertionActions;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import models.SessionData;

public class AssertionStepDefs {

    private AssertionActions assertionActions;

    // Dependency (session data) is injected by pico container
    public AssertionStepDefs(SessionData sessionData) {
        this.assertionActions = new AssertionActions(sessionData);
    }

    @Then("I check the response has the status code of {int}")
    public void iCheckTheResponseHasTheStatusCodeOf(int code) {
        assertionActions.validateResponseCode(code);
    }

    @And("The response contains the field \"([^\"]*)\" with the value \"([^\"]*)\"$")
    public void checkResponseBodyFieldHasValue(String field, String value) {
        assertionActions.theResponseHasTheCorrectStringValueInTheCorrectField(field, value);
    }

    @And("The response contains the string \"([^\"]*)\"$")
    public void responseContainsString(String value) {
        assertionActions.responseBodyContainsString(value);
    }

    @And("The response does not contain the string \"([^\"]*)\"$")
    public void responseDoesNotContainString(String value) {
        assertionActions.responseBodyDoesNotContainString(value);
    }

    @And("The response contains the field \"([^\"]*)\" which is not null$")
    public void responseContainsFieldWhichIsNotNull(String field) {
        assertionActions.responseContainsNotNullKeyValue(field);
    }

    @And("The response contains the field \"([^\"]*)\" which is null$")
    public void responseContainsFieldWhichIsNull(String field) {
        assertionActions.responseContainsNullKeyValue(field);
    }

}
