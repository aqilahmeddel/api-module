package api.steps;

import api.actions.AssertionActions;
import api.actions.PetStorePetApiActions;
import com.github.javafaker.Faker;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import models.SessionData;
import utils.GetSessionVariablesUtils;
import utils.sessionVariables.SessionVariable;

import static utils.enums.SessionVariablesKey.Name;

public class PetStorePetApiStepDefs {

    private PetStorePetApiActions petStorePetApiActions;
    private AssertionActions assertionActions;

    // Dependency (session data) is injected by pico container
    public PetStorePetApiStepDefs(SessionData sessionData) {
        this.assertionActions = new AssertionActions(sessionData);
        this.petStorePetApiActions = new PetStorePetApiActions(sessionData);
    }

    @Given("I create a new pet with a unique ID and the name \"([^\"]*)\" status \"([^\"]*)\"$")
    public void createNewPet(String name, String status) {
        petStorePetApiActions.createAPet(petStorePetApiActions.petPayload(name, status));
    }

    @Given("I create a new pet with a unique ID and name with status \"([^\"]*)\"$")
    public void createNewPetWithUniqueDetails(String status) {
        Faker faker = new Faker();
        String name = faker.name().fullName();
        SessionVariable.setSessionVariable(Name).to(name);

        petStorePetApiActions.createAPet(petStorePetApiActions.petPayload(name, status));
    }

    @Then("I retrieve the created pet$")
    public void retrieveCreatedPet() {
        petStorePetApiActions.retrieveThePetCreated();
    }

    @And("The response contains the field \"([^\"]*)\" with the correct value for the random \"([^\"]*)\"$")
    public void responseBodyHasCorrectValueForUniqueData(String field, String valueType) {

        switch (valueType) {
            case "ID":
                assertionActions.theResponseHasTheCorrectIntValueInTheCorrectField(field, Integer.parseInt(GetSessionVariablesUtils.getPetId()));
                break;
            case "Name":
                assertionActions.theResponseHasTheCorrectStringValueInTheCorrectField(field, GetSessionVariablesUtils.getName());
                break;
            default:
                throw new IllegalStateException("Unexpected value: " + valueType);

        }
    }

}

