package api.steps;

import api.actions.PostcodeApiActions;

import io.cucumber.java.en.Given;
import models.SessionData;

import java.util.List;

public class PostcodeApiStepDefs {

    private PostcodeApiActions postcodeApiActions;

    // Dependency (session data) is injected by pico container
    public PostcodeApiStepDefs(SessionData sessionData) {
        this.postcodeApiActions = new PostcodeApiActions(sessionData);
    }

    @Given("I send a post request to postcode.io to search for the following postcodes")
    public void iSendAPostRequestToPostcodeIoToSearchForTheFollowingPostcodes(List<String> postcodes) {
        postcodeApiActions.sendAPostRequestToPostcodeIoForMultiplePostcodes(postcodes);
    }

    @Given("I send a get request to postcode.io for the postcode {string}")
    public void iSendAGetRequestToPostcodeIoForThePostcode(String postcode) {
        postcodeApiActions.sendGetRequestToPostCodeIoForPostcodeOf(postcode);
    }
}
