package api.runners;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;


@RunWith(Cucumber.class)
@CucumberOptions(
        tags = "@api",
        features = "classpath:features/api",
        glue = {"api.steps"},
        plugin = {"pretty", "json:target/cucumber-report/cucumber.json"}
)
public class TestRunner {
}
