package utils;

public class PropertyUtils {

    private PropertyUtils() {
    }

    public static String getPostCodeIoUrl() {
        return Configuration.get("postcode.io.url");
    }

    public static String getPetstoreUrl() {
        return Configuration.get("petstore.url");
    }
}
