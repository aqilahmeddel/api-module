package utils;

import org.apache.commons.lang3.EnumUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import utils.enums.Environments;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class Configuration {

    private static Logger logger = LoggerFactory.getLogger(Configuration.class);
    private static Properties configuration;

    static {
        initConfig();
    }

    public static String get(String string) {
        return configuration.getProperty(string);
    }


    public static boolean checkEnvironmentSettingValid() {
        if (SetEnvironment.ENVIRONMENT != null) {
            return EnumUtils.isValidEnum(Environments.class, SetEnvironment.ENVIRONMENT);
        } else {
            return false;
        }
    }

    public static void initConfig() {
        String environment = System.getenv("CHART_NAME");
        String chartName = (environment != null) ? environment
                : (checkEnvironmentSettingValid()) ? SetEnvironment.ENVIRONMENT : "local";
        String propertyFile;

        if (chartName == null) {
            propertyFile = "properties/local.properties";
        } else {
            propertyFile = "properties/" + chartName + ".properties";
        }
        Properties localProperties = new Properties();
        loadProperties(localProperties, getClassPathResourceStream(propertyFile));
        configuration = localProperties;
    }

    private static void loadProperties(Properties props, InputStream inputStream) {
        try {
            props.load(inputStream);
        } catch (IOException ioexception) {
            logger.error(ioexception.getMessage());
        }
    }

    public static InputStream getClassPathResourceStream(String classpathResourceLoc) {
        return Thread.currentThread().getContextClassLoader().getResourceAsStream(classpathResourceLoc);
    }

    public static void setConfigurationProperty(String key, String value) {
        configuration.setProperty(key, value);
    }

}
