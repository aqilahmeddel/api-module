package utils;

import com.fasterxml.jackson.databind.JsonNode;
import com.github.fge.jackson.JsonLoader;
import com.mifmif.common.regex.Generex;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoField;
import java.util.Base64;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

public class CommonUtils {

    private CommonUtils() {
    }

    // <<<<<<<<<<<<<<< Date Utils >>>>>>>>>>>>>>>>>>>>

    static int getDayFromDate(String date, String format) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(format).withLocale(Locale.UK);
        return formatter.parse(date).get(ChronoField.DAY_OF_MONTH);
    }

    static int getMonthFromDate(String date, String format) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(format).withLocale(Locale.UK);
        return formatter.parse(date).get(ChronoField.MONTH_OF_YEAR);
    }

    static int getYearFromDate(String date, String format) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(format).withLocale(Locale.UK);
        return formatter.parse(date).get(ChronoField.YEAR);
    }

    //This is creating the current dateTimeStamp in the format below
    public static String generateCurrentDateTimeStampNoSSS() {
        String timeStamp = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'").format(new Date());
        return timeStamp.toString();
    }

    //This is creating the current dateTimeStamp in the format below
    public static String generateCurrentDateTimeStampSSSNoZ() {
        String timeStamp = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS").format(new Date());
        return timeStamp.toString();
    }

    //This is creating the current dateTimeStamp in the format below
    public static String generateCurrentDateTimeStampSSSAndZ() {
        String timeStamp = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'").format(new Date());
        return timeStamp.toString();
    }

    public static String pickRandomDataFromList() {
        int r = (int) (Math.random() * 9);
        String nationality = new String[]{"LIE", "ALB", "BEL", "BGR", "AUT", "POL", "CZE", "FRA", "HUN"}[r];
        return nationality.toString();
    }

    // <<<<<<<<<<<<<<< Random Data generated >>>>>>>>>>>>>>>>>>>>

    //This is creating a random regex value example
    public static String generateRandomRegexValue() {
        Generex generateRandomRegexValue = new Generex("[A-Z]{2}-[A-Za-z0-9]{4}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{4}-[0-9]");
        return generateRandomRegexValue.random().toString();
    }

    //This is creating a random alphaNumeric String of x length
    public static String generateRandomAlphaNumericString(int length) {
        String randomAlphaNumeric = RandomStringUtils.randomAlphanumeric(length);
        return randomAlphaNumeric.toString();
    }

    //This is creating a random alphabetic String of x length
    public static String generateRandomAlphabeticString(int length) {
        String randomAlphabetic = RandomStringUtils.randomAlphabetic(length);
        return randomAlphabetic.toString();
    }

    //This is creating a random numeric String of x length
    public static String generateRandomNumeric(int length) {
        String randomNumeric = RandomStringUtils.randomNumeric(length);
        return randomNumeric.toString();
    }

    // <<<<<<<<<<<<<<< Extras >>>>>>>>>>>>>>>>>>>>
    public void secondsWait(int seconds) throws IOException, InterruptedException {
        TimeUnit.SECONDS.sleep(seconds);
    }

    public static JsonNode getJsonNode(String jsonText)
            throws IOException {
        return JsonLoader.fromString(jsonText);
    } // getJsonNode(text) ends

    public static JsonNode getJsonNode(File jsonFile)
            throws IOException {
        return JsonLoader.fromFile(jsonFile);
    } // getJsonNode(File) ends

    public static JsonNode getJsonNode(URL url)
            throws IOException {
        return JsonLoader.fromURL(url);
    } // getJsonNode(URL) ends

    //This is going to get the JSON payload saved in the jsonData area and then making it into a string payload to submit for requests
    public String generateJsonFileToSubmit(String filePath, String jsonFile) throws ParseException, IOException {
        JSONParser parser = new JSONParser();

        Object object = parser.parse(new FileReader(System.getProperty("user.dir") + filePath + jsonFile));
        String exampleJsonPayload = object.toString();

        return exampleJsonPayload;
    }

    //This is going to get any file saved in the jsonData area and then converting it into a string to submit for requests
    public String obtainFileToSubmit(String filePath, String fileName) throws IOException {
        File file = new File(System.getProperty("user.dir") + filePath + fileName);

        return FileUtils.readFileToString(file, StandardCharsets.UTF_8);
    }

    //This is writing a particular string to a new file within a specific directory
    public void writeStringToFileNowTest(String filePath, String fileName, String data) throws IOException {

        File file = new File(System.getProperty("user.dir") + filePath + fileName);
        FileUtils.writeStringToFile(file, data, StandardCharsets.UTF_8, false);
    }

    //This is for turning a selected image into a base64String
    public String getImageToBase64String(final String imagePath) throws IOException {
        File file = new File(getClass().getClassLoader()
                .getResource(imagePath)
                .getFile());
        byte[] fileContent = FileUtils.readFileToByteArray(file);
        String encodedString = Base64.getEncoder().encodeToString(fileContent);

        return encodedString.toString();
    }

    //This is creating a new directory
    public boolean createNewDirectory(String directoryPath, String folderName) throws IOException {
        boolean newDirectory = new File(System.getProperty("user.dir") + directoryPath + folderName).mkdirs();
        return newDirectory;
    }

}
