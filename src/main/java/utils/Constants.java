package utils;

import io.restassured.http.Header;

public class Constants {

    private Constants() {
    }

    //Headers
    public static final Header JSON_CONTENT_TYPE_HEADER = new Header("Content-Type", "application/json");

    //Endpoints for APIs used
    public static final String PET_ENDPOINT = "pet/";

}
