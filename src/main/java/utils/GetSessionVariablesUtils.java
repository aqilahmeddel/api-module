package utils;

import utils.enums.SessionVariablesKey;
import utils.sessionVariables.SessionVariable;

public class GetSessionVariablesUtils {

    public static String getPetId() {
        return SessionVariable.sessionVariableCalled(SessionVariablesKey.Id).toString();
    }

    public static String getName() {
        return SessionVariable.sessionVariableCalled(SessionVariablesKey.Name).toString();
    }
}
