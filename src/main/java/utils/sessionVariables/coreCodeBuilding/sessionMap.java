package utils.sessionVariables.coreCodeBuilding;

import java.util.Map;

public interface sessionMap<K, V> extends Map<K, V> {
    Map<String, String> getMetaData();

    void addMetaData(String var1, String var2);

    void clearMetaData();

    void shouldContainKey(K var1);
}
