package utils.sessionVariables.coreCodeBuilding;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class TestVariableSession<K, V> extends ConcurrentHashMap implements sessionMap {
    private final Map<String, String> metadata = new ConcurrentHashMap();

    public TestVariableSession() {
    }

    public void shouldContainKey(Object key) {
        Object result = super.get(key);
        if (result == null) {
            throw new AssertionError("Session variable " + key + " expected but not found.");
        }
    }

    public Object put(Object key, Object value) {
        return value == null ? this.remove(key) : super.put(key, value);
    }

    public Map<String, String> getMetaData() {
        return NewMapSession.copyOf(this.metadata);
    }

    public void addMetaData(String key, String value) {
        this.metadata.put(key, value);
    }

    public void clearMetaData() {
        this.metadata.clear();
    }

    public void clear() {
        this.clearMetaData();
        super.clear();
    }
}

