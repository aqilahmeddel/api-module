package utils.sessionVariables;


import utils.sessionVariables.coreCodeBuilding.TestVariableSession;
import utils.sessionVariables.coreCodeBuilding.sessionMap;

public class SessionVariable {

    private static final ThreadLocal<TestVariableSession> testSessionThreadLocal = new ThreadLocal();

    public SessionVariable() {
    }

    public static SessionVariableSetter setSessionVariable(Object key) {
        return new SessionVariableSetter(key);
    }

    public static class SessionVariableSetter {
        final Object key;

        public SessionVariableSetter(Object key) {
            this.key = key;
        }

        public <T> void to(T value) {
            if (value != null) {
                SessionVariable.getCurrentSession().put(this.key, value);
            } else {
                SessionVariable.getCurrentSession().remove(this.key);
            }

        }
    }

    public static sessionMap<Object, Object> getCurrentSession() {
        if (testSessionThreadLocal.get() == null) {
            testSessionThreadLocal.set(new TestVariableSession());
        }

        return (sessionMap) testSessionThreadLocal.get();
    }

    public static <T> Object sessionVariableCalled(Object key) {
        return getCurrentSession().get(key);
    }

}
