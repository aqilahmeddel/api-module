package requests;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import models.SessionData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DeleteRequests {

    private Response response;
    Logger logger = LoggerFactory.getLogger(DeleteRequests.class.getName());
    private SessionData sessionData;

    public DeleteRequests(SessionData sessionData) {
        this.sessionData = sessionData;
    }

    //This is making a basic delete request
    public void deleteRequest(String url, String endpoint) {
        setResponse(
                RestAssured
                        .given()
                        .when()
                        .delete(url + endpoint));

        sessionData.setApiResponse(getResponse());

        logger.info("url used, {}", (url + endpoint));
    }

    private Response getResponse() {
        return response;
    }

    private void setResponse(Response response) {
        this.response = response;
    }
}
