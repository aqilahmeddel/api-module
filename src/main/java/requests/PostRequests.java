package requests;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import models.SessionData;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static utils.Constants.JSON_CONTENT_TYPE_HEADER;

public class PostRequests {

    private Response response;
    Logger logger = LoggerFactory.getLogger(PostRequests.class.getName());
    private SessionData sessionData;

    public PostRequests(SessionData sessionData) {
        this.sessionData = sessionData;
    }


    //This is a post request
    public void postRequest(JSONObject payload, String url, String endpoint) {
        setResponse(
                RestAssured
                        .given()
                        .when()
                        .body(payload.toString())
                        .header(JSON_CONTENT_TYPE_HEADER)
                        .post(url + endpoint));

        sessionData.setApiResponse(getResponse());
        System.out.println("url used: " + (url + endpoint));
        System.out.println("JSON object used: " + payload.toString());

        logger.info("url used, {}", (url + endpoint));
        logger.info("payload used, {}", payload);
    }


    private Response getResponse() {
        return response;
    }

    private void setResponse(Response response) {
        this.response = response;
    }
}
