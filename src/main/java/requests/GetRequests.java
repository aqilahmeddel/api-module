package requests;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import models.SessionData;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static utils.Constants.JSON_CONTENT_TYPE_HEADER;

public class GetRequests {

    private Response response;
    Logger logger = LoggerFactory.getLogger(GetRequests.class.getName());
    private SessionData sessionData;

    public GetRequests(SessionData sessionData) {
        this.sessionData = sessionData;
    }

    //This is making a get request
    public void getRequest(String url, String endpoint) {
        setResponse(
                RestAssured
                        .given()
                        .when()
                        .get(url + endpoint));

        sessionData.setApiResponse(getResponse());
        System.out.println("url used: " + (url + endpoint));
        logger.info("url used, {}", (url + endpoint));
    }

    //This is making a get request with query parameters
    public void getRequestWithQueryParams(String howManyQueryParamsRequired,
                                          String query1, String value1, String query2, String value2, String query3, String value3, String query4, String value4,
                                          String query5, String value5, String query6, String value6,
                                          String url, String endpoint) {

        switch (howManyQueryParamsRequired) {
            case "1":
                setResponse(
                        RestAssured
                                .given()
                                .queryParam(query1, value1)
                                .when()
                                .get(url + endpoint));

                sessionData.setApiResponse(getResponse());
                System.out.println("url used: " + (url + endpoint));
                System.out.println("query parameters: " + query1);
                System.out.println("values: " + value1);
                logger.info("url used, {}", (url + endpoint));
                logger.info("url used, {}", (url + endpoint));
                break;
            case "2":
                setResponse(
                        RestAssured
                                .given()
                                .queryParam(query1, value1)
                                .queryParam(query2, value2)
                                .when()
                                .get(url + endpoint));

                sessionData.setApiResponse(getResponse());

                System.out.println("url used: " + (url + endpoint));
                System.out.println("query parameters: " + query1 + query2);
                System.out.println("values: " + value1 + value2);
                logger.info("url used, {}", (url + endpoint));
                break;
            case "3":
                setResponse(
                        RestAssured
                                .given()
                                .queryParam(query1, value1)
                                .queryParam(query2, value2)
                                .queryParam(query3, value3)
                                .when()
                                .get(url + endpoint));

                sessionData.setApiResponse(getResponse());
                System.out.println("url used: " + (url + endpoint));
                System.out.println("query parameters: " + query1 + query2 + query3);
                System.out.println("values: " + value1 + value2 + value3);
                logger.info("url used, {}", (url + endpoint));
                break;
            case "4":
                setResponse(
                        RestAssured
                                .given()
                                .queryParam(query1, value1)
                                .queryParam(query2, value2)
                                .queryParam(query3, value3)
                                .queryParam(query4, value4)
                                .when()
                                .get(url + endpoint));

                sessionData.setApiResponse(getResponse());

                System.out.println("url used: " + (url + endpoint));
                System.out.println("query parameters: " + query1 + query2 + query3 + query4);
                System.out.println("values: " + value1 + value2 + value3 + value4);
                logger.info("url used, {}", (url + endpoint));
                break;
            case "5":
                setResponse(
                        RestAssured
                                .given()
                                .queryParam(query1, value1)
                                .queryParam(query2, value2)
                                .queryParam(query3, value3)
                                .queryParam(query4, value4)
                                .queryParam(query5, value5)
                                .when()
                                .get(url + endpoint));

                sessionData.setApiResponse(getResponse());
                System.out.println("url used: " + (url + endpoint));
                System.out.println("query parameters: " + query1 + query2 + query3 + query4 + query5);
                System.out.println("values: " + value1 + value2 + value3 + value4 + value5);
                logger.info("url used, {}", (url + endpoint));
                break;
            case "6":
                setResponse(
                        RestAssured
                                .given()
                                .queryParam(query1, value1)
                                .queryParam(query2, value2)
                                .queryParam(query3, value3)
                                .queryParam(query4, value4)
                                .queryParam(query5, value5)
                                .queryParam(query6, value6)
                                .when()
                                .get(url + endpoint));

                sessionData.setApiResponse(getResponse());
                System.out.println("url used: " + (url + endpoint));
                System.out.println("query parameters: " + query1 + query2 + query3 + query4 + query5 + query6);
                System.out.println("values: " + value1 + value2 + value3 + value4 + value5 + value6);
                logger.info("url used, {}", (url + endpoint));
                break;
            default:
                throw new IllegalStateException("Unexpected value: " + howManyQueryParamsRequired);
        }
    }

    //This is making a get request with a JSON body and content type application json
    public void getRequestWithJsonObject(String url, String endpoint, JSONObject payload) {
        setResponse(
                RestAssured
                        .given()
                        .when()
                        .body(payload.toString())
                        .header(JSON_CONTENT_TYPE_HEADER)
                        .get(url + endpoint));

        sessionData.setApiResponse(getResponse());
        System.out.println("url used: " + (url + endpoint));
        System.out.println("JSON object used: " + payload.toString());
        logger.info("url used, {}", (url + endpoint));
        logger.info("JSON object used, {}", payload);
    }

    //This is making a get request with headers
    public void getRequestWithHeaders(String header, String headerValue, String url, String endpoint) {
        setResponse(
                RestAssured
                        .given()
                        .header(header, headerValue)
                        .when()
                        .get(url + endpoint));

        sessionData.setApiResponse(getResponse());
        System.out.println("header used: " + header);
        System.out.println("header value used: " + headerValue);
        System.out.println("url used: " + (url + endpoint));
        logger.info("header used, {}", header);
        logger.info("header value used, {}", headerValue);
        logger.info("url used, {}", (url + endpoint));
    }

    private Response getResponse() {
        return response;
    }

    private void setResponse(Response response) {
        this.response = response;
    }
}
